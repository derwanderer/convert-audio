(defsystem "convert-audio-tests"
  :author "Michael Bucher"
  :license "GPL"
  :depends-on ("convert-audio"
	       "uiop"
	       "rove")
  :components ((:module "tests"
			:components
			((:file "aux")
			 (:file "channel")
			 (:file "macros")
			 (:file "find-files")
			 (:file "audio-files")
			 ;; (:file "main")
			 )))
  :description "Test system for convert-audio"
  :perform (test-op (op c) (symbol-call :rove :run c :style :dot)))
