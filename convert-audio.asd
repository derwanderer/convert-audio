(defsystem "convert-audio"
    :author "Michael Bucher"
    :license "GPL"
    :depends-on (:uiop
		 :alexandria
		 :chanl
		 :cl-fad
		 :unix-opts
		 :optima
		 :log4cl)
    :components ((:module "src"
			  :components
			  ((:file "convert-audio" :depends-on ("convert-audio-dir"))
			   (:module "convert-audio-dir"
				    :components
				    ((:file "aux")
				     (:file "channel")
				     (:file "macros" :depends-on ("aux"))
				     (:file "audio-files" :depends-on ("aux" "macros"))
				     (:file "convert" :depends-on ("aux" "channel" "audio-files"))
				     (:file "find-files" :depends-on ("aux" "audio-files" "channel"))
				     (:file "main" :depends-on ("aux" "macros" "channel" "audio-files" "find-files" "cli"))
				     (:file "cli" :depends-on ("aux"))
				     )
				    :pathname "convert-audio"))))
    :description "A toy project for testing out different approaches to workflow and coding styles/methods"
    :in-order-to ((test-op (test-op "convert-audio-tests"))))
