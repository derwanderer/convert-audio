(in-package :cl-user)
(defpackage convert-audio/convert
  (:use :cl
	:convert-audio/aux
	:convert-audio/channel
	:convert-audio/audio-files
	:optima)
  (:export :convert-fun
	   :starter
	   :stop))
(in-package :convert-audio/convert)

#+debug (declaim (optimize (debug 3) (safety 3)))

(defun convert-fun (options)
  (let ((new-type (get-option :output options)))
    (loop
       (let ((value (receive)))
	 (log:trace "converter received: ~a" value)
	 (match value
	   ((list id :quit)
	    (remove-process self)
	    (return))
	   ((list id :convert :file file)
	    (let ((file (open-audio-file file)))
	      (convert file new-type :options options)
	      (send id (list self :done))))
	   (_ nil))))))

(defun supervisor (options)
  (log:debug "start convert-server pid: ~a" self)
  (let ((number-of-processes (get-option :processes options))
	ready-processes
	busy-processes
	files-to-process
	quit)
    (dotimes (x number-of-processes)
      (declare (ignorable x))
      (log:trace "starting converter")
      (push (spawn (lambda () (convert-fun options))) ready-processes))
    (loop
       (log:trace "supervisor entering loop
file-to-process: ~a
ready-processes: ~a
busy-processes: ~a" files-to-process ready-processes busy-processes)
       (cond ((and ready-processes files-to-process)
	      (let ((file (pop files-to-process))
		    (process (pop ready-processes)))
		(log:trace "convert file: ~a" file)
		(send process (list self :convert :file file))
		(push process busy-processes)))
	     ((and quit (not files-to-process) (not busy-processes))
	      (mapcar (lambda (x) (send x (list self :quit)))
		      ready-processes)
	      (remove-process self)
	      (send quit (list self :quitting))
	      (log:debug "stop convert-server")
	      (return))
	     (t
	      (let ((value (receive)))
		(log:trace "supervisor received: ~a" value)
		(match value
		  ((list id :quit)
		   (setf quit id))
		  ((list id :done)
		   (push id ready-processes)
		   (setf busy-processes (remove id busy-processes)))
		  ((list id :file file)
		   (log:trace "received a file")
		   (push file files-to-process))
		  (_ nil))))))))

(defun starter (options)
  (spawn (lambda () (supervisor options))))

(defun stop (self pid)
  (send pid (list self :quit)))
