(in-package :cl-user)
(defpackage convert-audio/find-files
  (:use :cl
	:convert-audio/aux
	:convert-audio/audio-files
	:convert-audio/channel
	:optima)
  (:export :starter
	   :stop
	   :find-files-server))
(in-package :convert-audio/find-files)

#+debug (declaim (optimize (debug 3) (safety 3)))

(defun read-files (output-channel)
  "Reads *standard-input* for filenames/directories to use."
  (do ((line (read-line *standard-input* nil) (read-line *standard-input* nil)))
      ((null line) (send output-channel :done))
    (send output-channel (pathname line))))

(defun find-files-server (options)
  (log:debug "start find-files-server pid: ~a" self)
  (let ((type (symbol-to-type (get-option :input options))))
    (loop
       (let ((value (receive)))
	 (match value
	   ((list id :quit)
	    (send id (list self :quitting))
	    (remove-process self)
	    (log:debug "stop find-files-server")
	    (return))
	   ((list id :dir dir)
	    (handler-case
		(cl-fad:walk-directory dir
				       (lambda (file)
					 (send id (list self :file file)))
				       :test (lambda (x)
					       (string-equal
						type
						(pathname-type x))))
	      (error () (log:error "Couldn't walk directory."))))
	   (_ nil))))))

(defun starter (options)
  (spawn (lambda () (find-files-server options))))

(defun stop (self pid)
  (send pid (list self :quit)))
