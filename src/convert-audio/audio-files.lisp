(in-package :cl-user)
(defpackage convert-audio/audio-files
  (:use :cl
	:convert-audio/aux
	:convert-audio/macros)
  (:export :audio-file
	   :audio-name
	   :audio-type
	   :audio-file-path
	   :open-file
	   :open-audio-file
	   :convert
	   :play
	   :mp3-file
	   :flac-file
	   :wav-file))
(in-package :convert-audio/audio-files)

#+debug (declaim (optimize (debug 3) (safety 3)))

(defclass audio-file ()
  ((audio-name
    :initarg :audio-name
    :accessor audio-name
    :type string)
   (audio-file-path
    :initarg :audio-file-path
    :accessor audio-file-path
    :type pathname))
  (:documentation "A class to describe audio files."))

(defgeneric open-file (filepath type)
  (:documentation "Open the file specified by FILEPATH.
Returns a file-object defined by TYPE"))

(defun open-audio-file (filepath)
  "Open FILEPATH and return a audio-file object. Tries to guess the audio type from the file-extension."
  (let ((type (type-to-symbol (pathname-type filepath))))
    (open-file filepath type)))

(defgeneric convert (audio-file new-type &key options)
  (:documentation "Convert AUDIO-FILE to NEW-TYPE.
OPTIONS is a property list the key values being searched by eq."))

(defgeneric play (audio-file &key options)
  (:documentation "Play AUDIO-FILE
OPTIONS is a property list the key values being searched by eq."))

(defmethod open-file ((file pathname) type)
  (make-instance 'audio-file
		 :audio-file-path file
		 :audio-name (pathname-name file)))

(defun new-name (audio-file new-type)
  (let ((new-pathname (merge-pathnames (make-pathname :type (string-downcase
							     (mkstr new-type)))
				       (audio-file-path audio-file))))
    (open-file new-pathname new-type)))

(defmethod convert ((audio-file audio-file) new-type &key options)
  (let* ((new-obj (new-name audio-file new-type))
	 (old-name (namestring (audio-file-path audio-file)))
	 (new-name (namestring (audio-file-path new-obj)))
	 (command (uiop:escape-command (list "sox" "-V2" old-name new-name))))
;;    (log:debug "~a~%" command)
    (if (get-option :verbose options)
      (format t "~a~%" command))
    (if-program command new-obj)))

(defmethod play ((audio-file audio-file) &key options)
  (let* ((name (namestring (audio-file-path audio-file)))
	 (command (uiop:escape-command (list "play" name))))
    (log:debug "~a~%" command)
    (if (get-option :verbose options)
      (format t "~a~%" command))
    (run-program command)))

(generic-class flac mp3 wav)

(defmethod convert ((flac-file flac-file) (new-type (eql :wav)) &key options)
  (let* ((new-name (new-name flac-file new-type))
	 (command (uiop:escape-command
		   (list "flac" "-d" "-s"
			 (namestring (audio-file-path flac-file))))))
    (if (get-option :verbose options)
      (format t "~a~%" command))
    (if-program command new-name)))
