(in-package :cl-user)
(defpackage convert-audio/macros
  (:use :cl
	:convert-audio/aux)
  (:export :generic-class
	   :audio-file
	   :open-file
	   :run-program
	   :if-program
	   :when-option
	   :unless-option
	   :if-option
	   :it))
(in-package :convert-audio/macros)

#+debug (declaim (optimize (debug 3) (safety 3)))

(defun create-generic-class (name)
  (let ((class-name (symb name '-file)))
    `(progn
       (export ',class-name)
       (defclass ,class-name (audio-file) ()))))

(defun open-generic-file (name)
  (let ((class-name (symb name '-file))
	(key-name (symb-key name))
	(file (gensym)))
    `(defmethod open-file ((,file pathname) (type (eql ,key-name)))
       (make-instance ',class-name
		      :audio-name (pathname-name ,file)
		      :audio-file-path ,file))))

(defmacro generic-class (&rest types)
  `(progn
     ,@(mapcar (lambda (type) (create-generic-class type)) types)
     ,@(mapcar (lambda (type) (open-generic-file type)) types)))

(defun run-program (command)
  "Uses uiop:run-program to run COMMAND string.
stdout and stderr kept as is.
Only the shell return value is returned."
  (declare (type string command))
  (check-type command string)
  (multiple-value-bind (i1 i2 ret)
      (uiop:run-program command
			:output t
			:error-output t
			:ignore-error-status t)
    (declare (ignore i1 i2)
	     (type fixnum ret))
    ret))

(defmacro if-program (command then &optional else)
  "Use run-program to run COMMAND.
If successfull return THEN, if command failed return ELSE."
  (let ((ret (gensym)))
    `(let ((,ret (run-program ,command)))
       (declare (type fixnum ,ret))
       (if (zerop ,ret)
	   ,then
	   (progn (log:error "couldn't execute command '~a'~%return code: ~a" ,command ,ret)
		  ,else)))))

(defmacro when-option ((opt options) &body body)
  `(let ((it (get-option ,opt ,options)))
     (when it
       ,@body)))

(defmacro unless-option ((opt options) new-value)
  `(unless (get-option ,opt ,options)
     (setf-option ,opt ,new-value ,options)))

(defmacro if-option ((opt options) then else)
  `(let ((it (get-option ,opt ,options)))
     (if it
	 ,then
	 ,else)))
