(in-package :cl-user)
(defpackage convert-audio/channel
  (:use :cl
;;	:chanl
	)
  (:export :make-channel
	   :make-channels
;	   :channel
	   :send
	   :receive
	   :spawn
	   :remove-process
	   :stop-process
	   :self))
(in-package :convert-audio/channel)

#+debug (declaim (optimize (debug 3) (safety 3)))

(declaim (ftype (function (&key (:size (or null alexandria:positive-integer)))
			  queue-channel)))
(defun make-channel (&key size)
  "Make a new channel. By default is an unbounded channel. If a SIZE
  is given creates a bounded channel with buffer size SIZE."
  (if size
      (make-instance 'chanl:bounded-channel :size size)
      (make-instance 'chanl:unbounded-channel)))

(declaim (ftype (function (alexandria:positive-integer &key
						       (:size (or null
								  alexandria:positive-integer)))
			  list)
		make-channels))
(defun make-channels (number &key size)
  "Creates a list of channels bounded by SIZE. The list is a sequence
  and can be used with standard send/recv commands."
  (let (lst)
    (dotimes (x number)
      (push (make-channel :size size) lst))
    lst))

(defvar *processes* (make-hash-table :test #'equalp)
  "local variable to hold all active processes.")
(defvar *current-id* 0
  "local variable to keep track of the current pid")
(defvar self
  "local variable used to save the pid of the current process.")
(deftype pid ()
  "A type to describe the id of a process."
  'number)

(defclass process ()
  ((id :accessor id
       :initarg :id)
   (func :accessor func
	 :initarg :func)
   (channel :accessor channel
	    :initarg :channel
	    :initform (make-instance 'chanl:unbounded-channel)))
  (:documentation "A class to describe a process.
Used to group together the process function and a channel."))

(defun spawn (function)
  "Takes a FUNCTION of no arguments and creates a new process running this function.
Returns the processes pid."
  (let* ((id (incf *current-id*))
	 (channel (make-channel))
	 (process (make-instance 'process
				 :id id
				 :channel channel)))
    (setf (gethash id *processes*)
	  process)
    (setf (func process)
	  (chanl:pcall (lambda ()
		   (funcall function))
		 :initial-bindings `((self . (,id)))))
    id))

(defun remove-process (id)
  "Removes the process described by ID from the process-list.
Allows the process to be garbage collected once it stops running.
There is no guarantee how much longer messages can be send to this process.
Use with care."
  (remhash id *processes*))

(defun stop-process (self process)
  (send process (list self :quit)))

(defun send (process-id message)
  "Send MESSAGE to PROCESS-ID.
Process-id is a bit misnamed because it can be a process or a channel.
Always returns the message, gives no guarantee whether message was actually send."
  (handler-case
      (etypecase process-id
	(chanl:channel (chanl:send process-id message))
	(pid
	 (let ((channel (channel (gethash process-id *processes*))))
	   (chanl:send channel message))))
    (error () nil))
  message)

(defun receive (&optional channel)
  "Receives a message from CHANNEL or from the current process channel.
Blocks if no message is ready."
  (if channel
      (chanl:recv channel)
      (chanl:recv (channel (gethash self *processes*)))))
