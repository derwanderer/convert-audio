(in-package :cl-user)
(defpackage convert-audio/main
  (:documentation "this is a test!")
  (:use :cl
	:convert-audio/aux
	:convert-audio/macros
	:convert-audio/channel
	:convert-audio/find-files
	:convert-audio/audio-files
	:optima)
  (:shadow :starter
	   :stop)
  (:export :starter
	   :play
	   :convert
	   :audio-file))
(in-package :convert-audio/main)

#+debug (declaim (optimize (debug 3) (safety 3)))

(defun search-directories (directories producer consumer)
  (dolist (dir directories)
    (send producer (list consumer :dir dir))))

(defun read-stdin (producer consumer)
  (do ((line (read-line *standard-input* nil) (read-line *standard-input* nil)))
      ((null line) nil)
    (send producer (list consumer :dir line))))

(defun read-files (directories producer consumer)
  (if directories
      (search-directories directories producer consumer)
      (read-stdin producer consumer)))

(defun wait-for-process (channel id)
  (loop
       (let ((value (receive channel)))
	 (match value
	   ((list pid :quitting)
	    (cond ((= pid id)
		   (return))
		  (t (log :error "wrong process finished"))))
	   (_ nil)))))

(defun starter-fun (directories options)
  (let ((producer (convert-audio/find-files:starter options))
	(consumer (convert-audio/convert:starter options))
	(channel (make-channel)))
    (read-files directories producer consumer)
    ;;    (stop-process channel producer)
    (convert-audio/find-files:stop channel producer)
    (wait-for-process channel producer)
    ;;    (stop-process channel consumer)
    (convert-audio/convert:stop channel consumer)
    (wait-for-process channel consumer)))

(defun starter (directories options)
  (starter-fun directories options))
