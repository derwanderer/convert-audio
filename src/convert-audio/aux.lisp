(in-package :cl-user)
(defpackage convert-audio/aux
  (:use :cl)
  (:export :mkstr
	   :symb
	   :symb-key
	   :normalize-type
	   :type-to-symbol
	   :symbol-to-type
	   :make-options
	   :get-option
	   :setf-option
	   :transform-options))
(in-package :convert-audio/aux)

#+debug (declaim (optimize (debug 3) (safety 3)))

(declaim (ftype (function (&rest t) simple-string) mkstr))
(defun mkstr (&rest args)
  "Creates a string of the supplied ARGS
Taken from 'Let over Lambda'"
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

(declaim (ftype (function (&rest t) symbol) symb))
(defun symb (&rest args)
  "Creates a symbol of the supplied ARGS
Taken from 'Let over Lambda'"
  (values (intern (apply #'mkstr args))))

(declaim (ftype (function (&rest t) keyword) symb-key))
(defun symb-key (&rest args)
  "Creates a keyword of the supplied ARGS"
  (values (intern (apply #'mkstr args) :keyword)))

(declaim (ftype (function (string) string) normalize-type))
(defun normalize-type (type)
  "Creates a normalized version of the string TYPE"
  (string-downcase type))

(declaim (ftype (function (string) keyword) type-to-symbol))
(defun type-to-symbol (type)
  "Creates a symbol from the string TYPE"
  (symb-key (string-upcase type)))

(declaim (ftype (function (keyword) string) symbol-to-type))
(defun symbol-to-type (type)
  "Creates a type string from symbol TYPE"
  (normalize-type (mkstr type)))

(defun make-options ()
  (make-hash-table :test #'eql))

(defun get-option (opt options)
  (if (null options)
      '()
      (gethash opt options)))

(defun setf-option (opt value options)
  (setf (gethash opt options) value))

(defun transform-options (lst)
  (let ((options (make-options)))
    (labels ((rec (ls)
	       (cond ((null ls) options)
		     (t (let ((option (first ls))
			      (value (second ls))
			      (rest (cdr (cdr ls))))
			  (setf-option option value options)
			  (rec rest))))))
      (rec lst))))
