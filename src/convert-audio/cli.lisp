(in-package :cl-user)
(defpackage convert-audio/cli
  (:use :cl
	:convert-audio/aux
	:convert-audio/macros)
  (:export :parse-args))
(in-package :convert-audio/cli)

#+debug (declaim (optimize (debug 3) (safety 3)))

(opts:define-opts
  (:name :help
	 :description "print this help text"
	 :short #\h
	 :long "help")
  (:name :verbose
	 :description "print verbose output"
	 :short #\v
	 :long "verbose")
  (:name :output
	 :description "output file FORMAT"
	 :short #\o
	 :long "output"
	 :arg-parser #'type-to-symbol
	 :meta-var "FORMAT")
  (:name :input
	 :description "input file FORMAT"
	 :short #\i
	 :long "input"
	 :arg-parser #'type-to-symbol
	 :meta-var "FORMAT")
  (:name :processes
	 :description "processes used to convert audio files"
	 :arg-parser #'parse-integer
	 :meta-var "PROCESSES"
	 :short #\p
	 :long "processes"))

(defun unknown-option (condition)
  (log:warn "option is unknown: ~s" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defun parse-args (args)
  (multiple-value-bind (options free-args)
	(handler-case
	    (handler-bind ((opts:unknown-option #'unknown-option))
	      (opts:get-opts args))
	  (opts:missing-arg (condition)
	    (format t "fatal: option ~s needs an argument!~%"
		    (opts:option condition)))
	  (opts:arg-parser-failed (condition)
	    (format t "fatal: cannot parse ~s as argument of ~s~%"
		    (opts:raw-arg condition)
		    (opts:option condition)))
	  (opts:missing-required-option (con)
	    (format t "fatal: ~a~%" con)
	    (opts:exit 1)))
    (setf options (transform-options options))
    (if (get-option :help options)
	(progn
	  (opts:describe
	   ;;       :prefix "example—program to demonstrate unix-opts library"
	   ;;       :suffix "so that's how it works…"
	   :usage-of "convert-audio"
	   :args     "[AUDIO-FILES/DIRECTORIES]...")
	  (uiop:quit)))
    (unless-option (:processes options) 3)
    ;; the best tradeoff between time and CPU usage on my machine
    (unless-option (:input     options) :flac)
    (unless-option (:output    options) :mp3)
    (values options free-args)))
