(in-package :cl-user)
(defpackage :convert-audio
  (:use :cl
	:convert-audio/aux
	:convert-audio/cli
	:convert-audio/main)
  (:export :main
	   :open-audio-file
	   :convert
	   :play))
(in-package :convert-audio)

(defparameter log-file "/home/michael/.convert-audio/convert-audio.log")

(defun main (args)
  (declare (type list args))
  (check-type args list)
  (log:config :debug)
  (log:config :nopretty)
  (log:config :daily log-file)
  (multiple-value-bind (options dirs)
      (parse-args args)
    (log:trace "options: ~s   dirs: ~s~%" options dirs)
    (starter dirs
	     options)))
