# convert-audio
## A toy project for testing out different approaches to workflow and coding styles/methods

## How to use
The project is not yet in quicklisp, so copy the directories to a
place quicklisp or asdf can find it and just execute:


``` common-lisp
(ql:quickload :convert-audio)
```

or

``` common-lisp
(asdf:load-system :convert-audio)
```

to run the tests:

``` common-lisp
(asdf:test-system :convert-audio)
```

The project has two different interfaces:
- a command-line tool named convert-audio.ros
- functions exposed through the package :convert-audio
    - open-audio-file
	- convert
	- play

## dependencies
to use convert-audio you need sox and flac installed and in your $PATH.

## command-line interface
This is a simple roswell script used to convert audio-files.
Usage:

to list all available options:

``` shellsession
$ convert-audio.ros -h

Usage: convert-audio [-h|--help] [-v|--verbose] [-o|--output FORMAT]
                      [-i|--input FORMAT] [-p|--processes PROCESSES] [AUDIO-FILES/DIRECTORIES]...
Available options:

   -h, --help               print this help text
   -v, --verbose            print verbose output
   -o, --output FORMAT      output file FORMAT
   -i, --input FORMAT       input file FORMAT
   -p, --processes PROCESSES                           processes used to convert audio files
```

The default number of processes is 3 (which works best on my
computer, but may be different on different machines.), this is the
number of paralell conversions
Default input format is flac.
Default output format is mp3.
If no files/directories are given they are read from stdin, so this
can be used in pipes etc.

convert all flac files to mp3:

``` shellsession
$ convert-audio.ros -i flac -o mp3 ./music ./directories
```

## common lisp interface
there are three exposed functions for use from common lisp:

- open-audio-file
- convert
- play
- (for the script a main function is exposed but should not be called
  from common lisp directly)

`open-audio-file`: "opens" an audio file for usage with convert or play

``` common-lisp
convert-audio> (open-audio-file "some/audio/file.flac")
```

`convert`: used to convert an audio file to a different format

``` common-lisp
convert-audio> (convert (open-audio-file "some/audio/file.flac") :mp3)
```

`play`: play an audio file

``` common-lisp
convert-audio> (play (open-audio-file "some/audio/file.flac"))
```
