(in-package :cl-user)
(defpackage convert-audio/audio-files-tests
  (:use :cl
	:rove
	:convert-audio/audio-files))
(in-package :convert-audio/audio-files-tests)

(deftest audio-files
  (testing "audio-name"
    (ok (string= (audio-name (make-instance 'audio-file
					    :audio-name "foo"))
		 "foo")
	"get audio-name"))
  (testing "audio-file-path"
    (ok (equalp (audio-file-path (make-instance 'audio-file
						:audio-file-path #p"foo.bar"))
  		#p"foo.bar")
	"get audio-file-path"))
  (testing "open-file"
    (ok (eq (type-of (open-file #p"foo.bar" :baz))
	    'audio-file)
	"open audio-file")
    (ok (eq (type-of (open-file #p"foo.mp3" :mp3))
	    'mp3-file)
	"open mp3-file")
    (ok (eq (type-of (open-file #p"foo.flac" :flac))
	    'flac-file)
	"open flac-file")))
