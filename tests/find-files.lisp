(in-package :cl-user)
(defpackage convert-audio-tests/find-files
  (:use :cl
	:convert-audio/aux
	:convert-audio/find-files
	:convert-audio/channel)
  (:import-from :rove
		:deftest
		:testing
		:ok)
  (:import-from :optima :match))
(in-package :convert-audio-tests/find-files)

(deftest find-files
  (let ((options (convert-audio/aux:make-options)))
    (testing "find-files-server"
      (let* ((channel (make-channel))
	     (proc (starter options)))
	(setf-option :input :foo options)
	(send proc (list channel :quit))
	(ok
	 (match (receive channel)
	   ((list _ :quitting) t)
	   (_ nil))
	 "Starting and stopping the file server."))
      (uiop:with-current-directory ((asdf:system-source-directory :convert-audio))
	(let* ((channel (make-channel))
	       (proc (starter options)))
	  (setf-option :input :asd options)
	  (send proc (list channel :dir "."))
	  (send proc (list channel :quit))
	  (ok (match (receive channel)
		((list _ :file _) t)
		(_ nil))
	      "Receiving a file of type 'asd' in project directory.")))
      (uiop:with-current-directory ((asdf:system-source-directory :convert-audio))
	(let* ((channel (make-channel))
	       (proc (starter options)))
	  (setf-option :input :foobar options)
	  (send proc (list channel :dir "."))
	  (send proc (list channel :quit))
	  (ok (match (receive channel)
		((list _ :file _) nil)
		(_ t))
	      "Trying to find a non-existent file."))))))
