(in-package :cl-user)
(defpackage convert-audio/channel-tests
  (:use :cl
	:convert-audio/channel
	:rove))
(in-package :convert-audio/channel-tests)

(deftest channel
  (testing "spawn"
    (ok (numberp (spawn (lambda () (remove-process self))))
	"starting process and receiving pid"))
  (testing "send/receive"
    (ok (= 5
	   (let ((channel (make-channel)))
	     (let* ((receiver (spawn (lambda ()
				       (send channel (receive))
				       (remove-process self))))
		    (sender (spawn (lambda ()
				     (send receiver 5)
				     (remove-process self)))))
	       (receive channel))))
	"starting channel and process and sending a message between them")))
