(in-package :cl-user)
(defpackage convert-audio/aux-tests
  (:use :cl
	:convert-audio/aux
	:rove))
(in-package :convert-audio/aux-tests)

(deftest aux
  (testing "mkstr"
    (ok (string= (mkstr 'foo) "FOO")
	"one symbol")
    (ok (string= (mkstr 'foo 'bar) "FOOBAR")
	"two symbols")
    (ok (string= (mkstr 'foo :bar) "FOOBAR")
	"symbol and keyword")
    (ok (string= (mkstr 'foo "bar") "FOObar")
	"symbol and string"))
  (testing "symb-key"
    (ok (eq (symb-key "FOO") :foo)
	"upcase string")
    (ok (eq (symb-key "foo") :|foo|)
	"downcase string")
    (ok (eq (symb-key "FOO" "BAR") :foobar)
	"two strings"))
  (testing "normalize-type"
    (ok (string= (normalize-type "flac") "flac")
	"normal form")
    (ok (string= (normalize-type "FLAC") "flac")
	"non-normal form"))
  (testing "type-to-symbol"
    (ok (eq (type-to-symbol "flac") :flac)
	"downcase string")
    (ok (eq (type-to-symbol "MP3") :mp3)
	"upcase string"))
  (testing "symbol-to-type"
    (ok (string= (symbol-to-type :flac) "flac")
	"correct")
    (ok (string-not-equal (symbol-to-type :mp3) "mp")
	"not correct")))
