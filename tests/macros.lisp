(in-package :cl-user)
(defpackage convert-audio-tests/macros
  (:use :cl
	:rove
	:convert-audio/aux
	:convert-audio/macros)
  (:export :saved-package))
(in-package :convert-audio-tests/macros)

(defvar saved-package)

(setup
  (setf convert-audio-tests/macros:saved-package *package*)
  (in-package :convert-audio-tests/macros))

(deftest macros
  (testing "generic-class"
    (ok (expands '(generic-class mp3 flac)
		 `(progn
		    (progn (export 'mp3-file)
			   (defclass mp3-file (audio-file) ()))
		    (progn (export 'flac-file)
			   (defclass flac-file (audio-file) ()))
		    (defmethod open-file ((#:g pathname) (type (eql :mp3)))
		      (make-instance 'mp3-file
				     :audio-name (pathname-name #:g)
				     :audio-file-path #:g))
		    (defmethod open-file ((#:g pathname) (type (eql :flac)))
		      (make-instance 'flac-file
				     :audio-name (pathname-name #:g)
				     :audio-file-path #:g))))
	"expand generic-class"))
  (testing "if-program"
    (ok (expands '(if-program #1="ls" #2='success)
		 `(let ((#:g (run-program "ls")))
		    (declare (type fixnum #:g))
		    (if (zerop #:g)
			#2#
			(progn
			  (log:error "couldn't execute command '~a'~%return code: ~a" #1# #:g)
			  nil))))
	"expand if-program"))

  (testing "when-option"
    (ok (expands '(when-option (:help 'options) 'body)
		 `(let ((it (get-option :help 'options)))
		    (when it
		      'body)))
	"expand when-option"))

  (testing "unless-option"
(ok (expands '(unless-option (:help 'options) 't)
		 `(unless (get-option :help 'options)
		    (setf-option :help 't 'options)))
	"expand unless-option"))

  (testing "if-option"
    (ok (expands '(if-option (:help 'options) 'then 'else)
		 `(let ((it (get-option :help 'options)))
		    (if it
			'then
			'else)))
	"expand if-option")))

(teardown (setf *package* saved-package))
